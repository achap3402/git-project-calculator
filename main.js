const incomeSalary = document.getElementById('income-salary'),
      incomeFreelance = document.getElementById('income-freelance'),
      incomeExtra1 = document.getElementById('income-extra-1'),
      incomeExtra2 = document.getElementById('income-extra-2');
      
const costsFlat = document.getElementById('costs-flat'),
      costsHouseServices = document.getElementById('costs-house-services'),
      costsTransport = document.getElementById('costs-transport'),
      costsCredit = document.getElementById('costs-credit');

const totalMonthInput = document.getElementById('total-month'),
      totalDayInput = document.getElementById('total-day'),
      totalYearInput = document.getElementById('total-year');

const moneyBoxRange = document.getElementById('money-box-range'),
      accumulationInput = document.getElementById('accumulation'),
      spend = document.getElementById('spend');

const inputs = document.querySelectorAll('.input');

let totalMonth,
    totalDay,
    totalYear,
    accumulation = 0,
    totalPrecents = 0;

for(let input of inputs) {
    input.addEventListener('input', () => {
        countingAvailableMoney();
        calculationPrecents();
    })
}

const strToNum = str => str.value ? parseInt(str.value) : 0

const countingAvailableMoney = () => {
    const totalPerMonth = strToNum(incomeSalary) + strToNum(incomeFreelance) + strToNum(incomeExtra1) + strToNum(incomeExtra2);
    const totalCosts = strToNum(costsFlat) + strToNum(costsHouseServices) + strToNum(costsTransport) + strToNum(costsCredit);

    totalMonth = totalPerMonth - totalCosts;
    totalMonthInput.value = totalMonth;
}

moneyBoxRange.addEventListener('input', e => {
    const totalPrecentElement = document.getElementById('total-precents');
    totalPrecents = e.target.value;
    totalPrecentElement.innerHTML = totalPrecents;
    calculationPrecents();
});

const calculationPrecents = () => {
    if(totalMonth !== undefined) {
        accumulation = ((totalMonth * totalPrecents) / 100).toFixed();
        accumulationInput.value = accumulation;
        spend.value = totalMonth - accumulation;
    
        totalDay = (spend.value / 30).toFixed();
        totalDayInput.value = totalDay;
    
        totalYear = accumulation * 12;
        totalYearInput.value = totalYear;
    } else {
        accumulationInput.value = 0;
        spend.value = 0;
    }
}